# HttpRequest

### Build Http Request From Builder

Builder Support String or Array Settings.


```php
$reqStr =<<< DOC
GET /api/v1.1/page/search HTTP/1.1
Host: site.com
Cache-Control: no-cache
DOC;

$request = new \Poirot\Http\HttpRequest(
    new BuildHttpRequest(BuildHttpRequest::parseWith($reqStr))
);
```

## Headers

```php
HeaderFactory::of('WWW-Authenticate: Basic realm="admin_panel"');
HeaderFactory::of(['WWW-Authenticate', 'Basic realm="admin_panel"']);
// options of specific header as plugin
HeaderFactory::of('WWW-Authenticate' => ['header_line' => 'Basic realm="admin_panel"']);
```
