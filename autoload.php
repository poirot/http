<?php
namespace Poirot\Http;

function autoload($class) {
    if (strpos($class,__NAMESPACE__ . '\\') !== 0)
        return false;

    $file = __DIR__
        . DIRECTORY_SEPARATOR
        . str_replace('\\',DIRECTORY_SEPARATOR, substr($class,strlen(__NAMESPACE__) + 1))
        . '.php';

    if (is_file($file))
        require_once $file;
}

spl_autoload_register(__NAMESPACE__ . '\\autoload');
