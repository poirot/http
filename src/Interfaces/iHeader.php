<?php
namespace Poirot\Http\Interfaces;

use Poirot\Std\Interfaces\Struct\iDataOptions;

/**
 * Multiple Header Values separated by , (comma)
 *
 */
interface iHeader 
    extends iDataOptions
{
    /**
     * Set Header Label
     *
     * @param string $label
     *
     * @return $this
     * @throws \Exception Invalid header name
     */
    function setLabel($label);
    
    /**
     * Get Header Label
     * @ignored not consider as data options
     * 
     * @return string
     */
    function getLabel();
    
    /**
     * Get Field Value As String
     *
     * @return string
     */
    function renderValueLine();

    /**
     * Represent Header As String
     *
     * - filter values just before output
     *
     * from rfc:
     * Header fields are lines composed of a field name, followed by a colon
     * (":"), followed by a field body, and terminated by CRLF.
     *
     * @return string
     */
    function render();
}
