<?php
namespace Poirot\Http\Interfaces;

use Poirot\Stream\Interfaces\iStreamable;
use Psr\Http\Message\StreamInterface;

use Poirot\Std\Interfaces\Pact\ipMetaProvider;


// TODO render methods can move outside as functional; get parsedObject array or struct  

interface iHttpMessage 
    extends ipMetaProvider
{
    /**
     * Set Version
     *
     * @param string $version
     *
     * @return $this
     */
    function setVersion($version);

    /**
     * Get Version
     *
     * @return string
     */
    function getVersion();

    /**
     * Set message headers or headers collection
     *
     * ! HTTP messages include case-insensitive header
     *   field names
     *
     * ! headers may contains multiple values, such as cookie
     *
     * @param array|iHeaders $headers
     *
     * @return $this
     */
    function setHeaders($headers);

    /**
     * Get Headers collection
     *
     * @param callable $glueFunctor function($headersObject)
     *
     * @return iHeaders
     */
    function headers($glueFunctor = null);

    /**
     * Set Message Body Content
     *
     * @param string|iStreamable|null $content
     *
     * @return $this
     */
    function setBody($content);

    /**
     * Get Message Body Content
     *
     * @return string|iStreamable|null
     */
    function getBody();

    /**
     * Render Headers
     *
     * - include line break at bottom
     *
     * @return string
     */
    function renderHeaders();

    /**
     * Render Http Message To String
     *
     * - render header
     * - render body
     *
     * @return string
     */
    function render();
}
