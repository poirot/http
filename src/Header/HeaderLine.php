<?php
namespace Poirot\Http\Header;


/*
// Accept: audio/mp3; q=0.2; version=0.5, audio/basic+mp3

$h = new \Poirot\Http\Header\HeaderLine(
    'Accept'
    , [
        ['audio/mp3', 'q'=>'0.2', 'version'=>'0.5']
        , 'audio/basic+mp3'
        // or
        , ['audio/basic+mp3']
    ]
);
*/

class HeaderLine 
    extends aHeaderHttp
{
    /**
     * AbstractStruct constructor.
     *
     * @param string $label
     * @param null|array|\Traversable $data
     */
    function __construct($label, $data = null)
    {
        $this->setLabel($label);

        parent::__construct($data);
    }
}
