<?php
namespace Poirot\Http;

use Poirot\Std\Interfaces\Struct\iDataMean;
use Poirot\Std\Struct\DataMean;

use Poirot\Http\Header\CollectionHeader;
use Poirot\Http\Header\FactoryHttpHeader;
use Poirot\Http\Interfaces\iHeader;
use Poirot\Http\Interfaces\iHeaders;
use Poirot\Http\Interfaces\iHttpMessage;
use Poirot\Stream\Interfaces\iStreamable;
use Psr\Http\Message\StreamInterface;


abstract class aHttpMessage
    implements iHttpMessage
{
    const Vx1_0 = '1.0';
    const Vx1_1 = '1.1';

    /** @var DataMean */
    protected $meta;

    protected $version = '1.1';
    /** @var iHeaders */
    protected $headers;
    /** @var string|StreamInterface */
    protected $body;

    
    
    /**
     * @return iDataMean
     */
    function meta()
    {
        if (!$this->meta)
            $this->meta = new DataMean;

        return $this->meta;
    }

    /**
     * Render Headers
     *
     * - include line break at bottom
     *
     * @return string
     */
    function renderHeaders()
    {
        $return = '';
        /** @var iHeader $header */
        foreach ($this->headers() as $header)
            $return .= $header->render();

        $return .= "\r\n";
        return $return;
    }

    /**
     * Render Http Message To String
     *
     * - render header
     * - render body
     *
     * @return string
     */
    function render()
    {
        $return = $this->renderHeaders();

        $body = $this->getBody();
        if ($body instanceof iStreamable) {
            if ($body->resource()->isSeekable()) $body->rewind();
            while (!$body->isEOF())
                $return .= $body->read(24400);
        } else {
            $return .= $body;
        }

        return $return;
    }

    // Options:

    /**
     * Set Version
     *
     * @param string $ver
     *
     * @return $this
     */
    function setVersion($ver)
    {
        $this->version = (string) $ver;
        return $this;
    }

    /**
     * Get Version
     *
     * @return string
     */
    function getVersion()
    {
        if (empty($this->version))
            $this->version = self::Vx1_1;
        
        return $this->version;
    }

    /**
     * Set message headers or headers collection
     *
     * ! HTTP messages include case-insensitive header
     *   field names
     *
     * ! headers may contains multiple values, such as cookie
     *
     * @param array|iHeaders $headers
     *
     * @return $this
     */
    function setHeaders($headers)
    {
        if ($headers instanceof iHeaders) {
            $tHeaders = array();
            foreach($headers as $h)
                $tHeaders[] = $h;
            $headers = $tHeaders;
        }

        if (! is_array($headers) )
            throw new \InvalidArgumentException;

        foreach ($headers as $label => $h) {
            if (! $h instanceof iHeader ) {
                if (is_int($label) && is_string($h))
                    // ['X-Powered-By: PHP/5.5.9-1ubuntu4.16', ]
                    // in form of headers_list()
                    $valuable = $h;
                else
                    $valuable = array($label => $h);
                        
                $h = FactoryHttpHeader::of($valuable);
            }

            $this->headers()->insert($h);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    function headers($glueCallback = null)
    {
        if (! $this->headers )
            $this->headers = new CollectionHeader;

        if ($glueCallback) {
            if (null !== $r = call_user_func($glueCallback, $this->headers))
                return $r;

            return $this;
        }

        return $this->headers;
    }

    /**
     * Set Message Body Content
     *
     * @param string|iStreamable|null $content
     *
     * @return $this
     */
    function setBody($content)
    {
        if (! $content instanceof iStreamable )
            ## Instead Of StreamInterface must convert to string
            $content = (string) $content;

        $this->body = $content;
        return $this;
    }

    /**
     * Get Message Body Content
     *
     * @return string|iStreamable|null
     */
    function getBody()
    {
        return $this->body;
    }
}
