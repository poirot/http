<?php
namespace Poirot\Http\HttpMessage\Response\Plugin;

use function Poirot\Http\Header\renderHeader;
use Poirot\Http\Interfaces\iHeader;
use Poirot\Stream\Interfaces\iStreamable;


class PhpServer 
    extends aPluginResponse
{
    protected $isHeadersSent;
    protected $isContentSent;


    /**
     * Send HTTP response
     *
     * @return $this
     * @throws \Exception
     */
    function send()
    {
        $this
            ->sendHeaders()
            ->sendContent();


        if (function_exists('fastcgi_finish_request'))
            fastcgi_finish_request();
        else
            $this->_closeOutputBuffers(0, true);

        return $this;
    }

    /**
     * Send HTTP headers
     *
     * @return $this
     * @throws \Exception headers sent
     */
    function sendHeaders()
    {
        if ($this->isHeadersSent())
            throw new \Exception('Headers was sent.');

        
        \Poirot\Http\Response\httpResponseCode($this->getMessageObject()->getStatusCode());

        $httpHeaders = $this->getMessageObject()->headers();
        
        /** @var iHeader $header */
        foreach ($httpHeaders as $header)
            header($header->render(), false, $this->getMessageObject()->getStatusCode());


        $this->isHeadersSent = true;
        return $this;
    }

    /**
     * Send content
     *
     * @return $this
     * @throws \Exception
     */
    function sendContent()
    {
        if ($this->isContentSent())
            return $this;

        $body = $this->getMessageObject()->getBody();
        ob_start();
        if ($body instanceof iStreamable) {
            while (! $body->isEOF() )
                echo $r = $body->read(24400);
            ob_end_flush();
            flush();
            ob_start();
        } else {
            echo $body;
        }
        ob_end_flush();
        flush();
        
        $this->isContentSent = true;
        return $this;
    }

    /**
     * @return bool
     */
    function isHeadersSent()
    {
        return headers_sent() || $this->isHeadersSent;
    }

    /**
     * @return bool
     */
    function isContentSent()
    {
        return $this->isContentSent;
    }


    // Helpers

    /**
     * Modifies the response so that it conforms to the rules defined for a 304 status code.
     *
     * This sets the status, removes the body, and discards any headers
     * that MUST NOT be included in 304 responses.
     *
     * @return $this
     *
     * @see http://tools.ietf.org/html/rfc2616#section-10.3.5
     *
     * @final
     */
    function setNotModified()
    {
        $this->getMessageObject()->setStatusCode(304);
        $this->getMessageObject()->setBody(null);

        // remove headers that MUST NOT be included with 304 Not Modified responses
        foreach ([
            'Allow', 'Content-Encoding', 'Content-Language',
             'Content-Length', 'Content-MD5', 'Content-Type',
             'Last-Modified'] as $header)
        {
            $this->getMessageObject()
                ->headers()->del($header);
        }


        return $this;
    }

    /**
     * Sets the Last-Modified HTTP header with a DateTime instance.
     *
     * Passing null as value will remove the header.
     *
     * @param \DateTimeInterface|null $date
     *
     * @return $this
     */
    function setLastModified(\DateTimeInterface $date = null)
    {
        throw new \Exception(__METHOD__.' Not Implemented');
    }

    /**
     * Returns the Last-Modified HTTP header as a DateTime instance.
     *
     * @throws \RuntimeException When the HTTP header is not parseable
     */
    function getLastModified(): ?\DateTimeInterface
    {
        throw new \Exception(__METHOD__.' Not Implemented');
    }

    /**
     * Returns the value of the Expires header as a DateTime instance.
     *
     */
    function getExpires(): ?\DateTimeInterface
    {
        throw new \Exception(__METHOD__.' Not Implemented');
    }

    /**
     * Sets the Expires HTTP header with a DateTime instance.
     *
     * Passing null as value will remove the header.
     *
     * @return $this
     */
    function setExpires(\DateTimeInterface $date = null): object
    {
        throw new \Exception(__METHOD__.' Not Implemented');
    }


    // ..

    /**
     * Cleans or flushes output buffers
     *
     */
    protected function _closeOutputBuffers(int $targetLevel, bool $flush)
    {
        $status = ob_get_status(true);
        $level = \count($status);
        $flags = PHP_OUTPUT_HANDLER_REMOVABLE | ($flush ? PHP_OUTPUT_HANDLER_FLUSHABLE : PHP_OUTPUT_HANDLER_CLEANABLE);

        while ($level-- > $targetLevel && ($s = $status[$level]) && (!isset($s['del']) ? !isset($s['flags']) || ($s['flags'] & $flags) === $flags : $s['del'])) {
            if ($flush) {
                ob_end_flush();
            } else {
                ob_end_clean();
            }
        }
    }
}
