<?php
namespace Poirot\Http\HttpMessage\Response;

use Poirot\Http\Interfaces\iHttpResponse;
use Poirot\Std\aConfigurable;
use Psr\Http\Message\ResponseInterface;


class BuildHttpResponse
    extends aConfigurable
{
    protected $version;
    protected $status_code;
    protected $status_reason;
    protected $headers;
    protected $body;

    /**
     * Build Http Request
     *
     * @param iHttpResponse $response Response Instance to build
     *
     * @return iHttpResponse
     */
    function build(iHttpResponse $response)
    {
        ($this->version       === null) ?: $response->setVersion($this->version);
        ($this->status_code   === null) ?: $response->setStatusCode($this->status_code);
        ($this->status_reason === null) ?: $response->setStatusReason($this->status_reason);
        ($this->headers       === null) ?: $response->setHeaders($this->headers);
        ($this->body          === null) ?: $response->setBody($this->body);

        return $response;
    }

    /**
     * Build Object With Provided Options
     *
     * @param array $options Associated Array
     * @param bool $throwException Throw Exception On Wrong Option
     *
     * @return $this
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    function with(array $options, $throwException = false)
    {
        foreach ($options as $key => $val) {
            $key = strtolower($key);
            $this->$key = $val;
        }
    }


    // Implement Configurable

    /**
     * Load Build Options From Given Resource
     *
     * - usually it used in cases that we have to support
     *   more than once configure situation
     *   [code:]
     *     Configurable->with(Configurable::withOf(path\to\file.conf))
     *   [code]
     *
     *
     * @param array|mixed $optionsResource
     * @param array       $_
     *        usually pass as argument into ::with if self instanced
     *
     * @throws \InvalidArgumentException if resource not supported
     * @return array
     */
    static function parseWith($optionsResource, array $_ = null)
    {
        $optionsResource = parent::parseWith($optionsResource, $_);
        if ( is_string($optionsResource) )
            $optionsResource = \Poirot\Http\parseResponseFromString($optionsResource);
        elseif ( $optionsResource instanceof ResponseInterface )
            $optionsResource = \Poirot\Http\parseResponseFromPsr($optionsResource);

        return $optionsResource;
    }

    /**
     * Is Configurable With Given Resource
     *
     * @param mixed $optionsResource
     *
     * @return boolean
     */
    static function isConfigurableWith($optionsResource)
    {
        return (
            $optionsResource instanceof ResponseInterface
            || is_string($optionsResource)
            || parent::isConfigurableWith($optionsResource)
        );
    }
}
