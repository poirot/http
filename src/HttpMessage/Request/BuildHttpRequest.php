<?php
namespace Poirot\Http\HttpMessage\Request;

use Poirot\Http\Interfaces\iHttpRequest;
use Poirot\Std\aConfigurable;
use Psr\Http\Message\RequestInterface;


class BuildHttpRequest
    extends aConfigurable
{
    protected $protocol;
    protected $method;
    protected $host;
    protected $version;
    protected $target;
    protected $headers;
    protected $body;


    /**
     * Build Http Request
     *
     * @param iHttpRequest $request Request Instance to build
     */
    function build(iHttpRequest $request)
    {
        switch (1)
        {
            case ($this->protocol !== null): $request->setProtocol($this->protocol);
            case ($this->method   !== null): $request->setMethod($this->method);
            case ($this->host     !== null): $request->setHost($this->host);
            case ($this->version  !== null): $request->setVersion($this->version);
            case ($this->target   !== null): $request->setTarget($this->target);
            case ($this->headers  !== null): $request->setHeaders($this->headers);
            case ($this->body     !== null): $request->setBody($this->body);
        }
    }

    /**
     * Build Object With Provided Options
     *
     * @param array $options Associated Array
     * @param bool $throwException Throw Exception On Wrong Option
     *
     * @return $this
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    function with(array $options, $throwException = false)
    {
        foreach ($options as $key => $val) {
            $key = strtolower($key);
            $this->$key = $val;
        }
    }


    // Implement Configurable

    /**
     * Load Build Options From Given Resource
     *
     * - usually it used in cases that we have to support
     *   more than once configure situation
     *   [code:]
     *     Configurable->with(Configurable::withOf(path\to\file.conf))
     *   [code]
     *
     *
     * @param array|mixed $optionsResource
     * @param array       $_
     *        usually pass as argument into ::with if self instanced
     *
     * @throws \InvalidArgumentException if resource not supported
     * @return array
     */
    static function parseWith($optionsResource, array $_ = null)
    {
        $optionsResource = parent::parseWith($optionsResource, $_);
        if ( is_string($optionsResource) )
            $optionsResource = \Poirot\Http\parseRequestFromString($optionsResource);
        elseif ($optionsResource instanceof RequestInterface)
            $optionsResource = \Poirot\Http\parseRequestFromPsr($optionsResource);

        return $optionsResource;
    }

    /**
     * Is Configurable With Given Resource
     *
     * @param mixed $optionsResource
     *
     * @return boolean
     */
    static function isConfigurableWith($optionsResource)
    {
        return (
            is_string($optionsResource)
            || $optionsResource instanceof RequestInterface
            || parent::isConfigurableWith($optionsResource)
        );
    }
}
